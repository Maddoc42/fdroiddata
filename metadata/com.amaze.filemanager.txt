Categories:Office
License:GPLv3
Web Site:
Source Code:https://github.com/arpitkh96/AmazeFileManager
Issue Tracker:https://github.com/arpitkh96/AmazeFileManager/issues

Auto Name:Amaze File Manager
Summary:File Manager
Description:
Light and smooth file manager following the Material Design
guidelines.

Features:

- Basic features like cut, copy, delete, compress, extract etc. easily accessible
- Work on multiple tabs at same time
- Multiple themes with cool icons
- Navigation drawer for quick navigation
- App Manager to open, backup, or directly uninstall any app
- Quickly access history, access bookmarks or search for any file
- Root explorer for advanced users
.

Repo Type:git
Repo:https://github.com/arpitkh96/AmazeFileManager

Build:1.3.0,4
    disable=ucmfix
    commit=ucmfix

Build:1.3.1,5
    commit=361b80e16ac0e151a69a8f0c3c9bb65de2a74ad1
    gradle=yes
    srclibs=RootTools@3.4
    rm=libs/RootTools.jar
    prebuild=cp -fR $$RootTools$$/RootTools/src/main/java/com src/ && \
        sed -i -e "/apply plugin: 'android'/d" build.gradle

Build:1.4.0,6
    commit=v1.4
    gradle=yes
    srclibs=RootTools@3.4
    rm=libs/*
    prebuild=cp -fR $$RootTools$$/RootTools/src/main/java/com src/

Build:1.4.2,8
    commit=v1.4.2
    gradle=yes
    srclibs=RootTools@3.4
    rm=libs/*
    prebuild=cp -fR $$RootTools$$/RootTools/src/main/java/com src/

Build:1.5,9
    commit=v1.5
    gradle=yes
    srclibs=RootTools@3.4
    rm=libs/*
    prebuild=cp -fR $$RootTools$$/RootTools/src/main/java/com src/

Build:1.5.1,10
    commit=v1.5.1
    gradle=yes
    srclibs=RootTools@3.4
    rm=libs/*
    prebuild=cp -fR $$RootTools$$/RootTools/src/main/java/com src/

Build:1.5.2,11
    disable=7ZipBindings
    commit=424aa781773d9bd6c5b95d49f7cd881150125e7c
    gradle=fdroid
    srclibs=RootTools@3.4,7ZipJBindings@Release-4.65-1.04rc-extr-only
    rm=libs/*.jar
    prebuild=cp -fR $$RootTools$$/RootTools/src/main/java/com src/main/java/ && sed -i -e '/playCompile/d' -e '/Properties/,$d' build.gradle && \
        pushd $$7ZipJBindings$$ && cmake . && make && make package

#Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.5.1
Current Version Code:10

