Categories:Science & Education,Office,Navigation
License:GPLv3
Web Site:
Source Code:https://github.com/arkon/CDFLabs
Issue Tracker:https://github.com/arkon/CDFLabs/issues

Auto Name:CDF Labs
Summary:Check availibilty of computers at CDFLabs
Description:
Allows Computer Science students at University of Toronto St George to
quickly check how many machines are available in the various CDF
computer labs in Bahen and on NX.
.

Repo Type:git
Repo:https://github.com/arkon/CDFLabs

Build:1.1,11
    commit=5899be13b2480ff05bfb850c4156cb1ab374df76
    subdir=app
    gradle=yes
    rm=app/libs/*jar
    prebuild=sed -i -e '/support-v4/acompile "org.jsoup:jsoup:1.7.3"' build.gradle

Build:2.0,14
    commit=6124e55ebce37c105efa324845cd7b89770bc074
    subdir=app
    gradle=yes
    rm=app/libs/*jar

Build:2.0.1,15
    commit=715f7c73d60419bc019c8351dcf5d809c7e058d9
    subdir=app
    gradle=yes
    rm=app/libs/*jar

Build:2.0.2,16
    commit=36379b31d8b5f892fff67cddad07207537052299
    subdir=app
    gradle=yes
    rm=app/libs/*jar

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:2.0.2
Current Version Code:16

