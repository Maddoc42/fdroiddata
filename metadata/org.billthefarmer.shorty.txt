Categories:Multimedia
License:GPLv3
Web Site:https://github.com/billthefarmer/shorty/blob/master/README.md
Source Code:https://github.com/billthefarmer/shorty
Issue Tracker:https://github.com/billthefarmer/shorty/issues

Auto Name:Shorty
Summary:Shortcuts for Intent Radio
Description:
Create shortcuts for Intent Radio.

* Create shortcuts from the app or the android launcher.
* Defaults to BBC Radio 4 if no data is entered.
* Lookup stations from a list.
* Values and list saved between uses.
* Save and restore the list from the device file system.
.

Repo Type:git
Repo:https://github.com/billthefarmer/shorty

Build:1.0,1
    commit=v1.0

Build:1.01,101
    commit=v1.01

Build:1.02,102
    commit=v1.02

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.02
Current Version Code:102

